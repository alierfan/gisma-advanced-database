-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2023 at 07:02 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
CREATE DATABASE IF NOT EXISTS `sales-app`;
--
USE `sales-app`;
-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email`, `phone`, `address`) VALUES
(1, 'Kaycee', 'Kling', 'mayra10@yahoo.com', '+1-283-705-3047', '2059 Ullrich Mill\nSteuberland, NH 87846-3949'),
(2, 'Sigmund', 'Quitzon', 'marvin.rocky@roob.org', '(207) 920-6203', '2976 Maynard Center\nMakenzieport, TN 60073'),
(3, 'Kenny', 'Heller', 'paxton.howell@ward.com', '434.222.3213', '777 Hahn Court Apt. 407\nKochside, GA 00968-3104'),
(4, 'Reynold', 'Beier', 'mercedes.greenholt@fisher.com', '(623) 971-5596', '788 Darian Light\nGreenberg, WY 57476'),
(5, 'Chaz', 'Leffler', 'nkeebler@pfeffer.net', '1-220-953-8007', '19555 Angie Island\nJoyshire, AZ 06211'),
(6, 'Isabella', 'Crona', 'makenna.senger@jerde.com', '+1-828-348-3865', '38183 Beier Turnpike Suite 153\nSawaynchester, NV 98684-5366'),
(7, 'Ezequiel', 'Hand', 'jacobs.pearline@hotmail.com', '669.729.2027', '8401 Nicklaus Trafficway\nLake Karelle, MI 93355-8527'),
(8, 'Isabel', 'Heaney', 'alan.mitchell@rempel.net', '973-309-1158', '987 Lonie Grove\nCasperborough, MO 70154'),
(9, 'Isobel', 'Ankunding', 'maynard01@stehr.com', '248-871-3614', '98420 Hauck Course Apt. 048\nEast Brandy, WI 55000'),
(10, 'Emory', 'Trantow', 'whansen@herman.com', '+1-901-485-2718', '562 Will Summit Suite 775\nNaderville, VA 45646-8270'),
(11, 'Mack', 'Tromp', 'mcremin@hotmail.com', '+1-352-413-5637', '51646 Jaskolski Walk\nNew Mateo, NE 34668-2021'),
(12, 'Fay', 'Bins', 'dario.medhurst@gmail.com', '479.492.2910', '8999 Xavier Rest Apt. 203\nPort Aricport, OR 18994'),
(13, 'Margaret', 'Schneider', 'ana.durgan@donnelly.biz', '940.999.7986', '893 Cathryn Row Suite 940\nMartaland, ND 09476'),
(14, 'Rita', 'Morar', 'pfarrell@towne.com', '+1-445-542-1771', '41790 Reynolds Field Suite 411\nNorth Hailiemouth, IN 29493'),
(15, 'Rhiannon', 'Jones', 'ethel12@pagac.org', '1-224-630-0979', '1710 Morar Freeway\nLake Lontown, OK 08657'),
(16, 'Reed', 'Padberg', 'victor70@yahoo.com', '351.289.6303', '81918 Judd Drives Apt. 740\nIsaiton, SC 93235'),
(17, 'Lenny', 'Russel', 'clowe@gmail.com', '1-954-269-5362', '84968 Beier Curve\nLake Madelynnport, NV 71233'),
(18, 'Lambert', 'Considine', 'udurgan@gmail.com', '1-818-293-1587', '13818 Schneider Plaza Suite 078\nHillfurt, WY 10095'),
(19, 'Merlin', 'Hahn', 'ellie37@veum.com', '+1-347-360-1233', '97120 Jovan Summit\nEast Shanna, MD 28570'),
(20, 'Romaine', 'Wolff', 'ggoyette@hotmail.com', '+1-248-999-2053', '80278 O\'Kon Estate Apt. 816\nLake Marcelinaville, HI 67445-8850'),
(21, 'Jamarcus', 'Baumbach', 'kilback.norbert@hegmann.com', '341-669-1197', '76298 Crist Squares\nNew Howardfurt, PA 35663-9716'),
(22, 'Vaughn', 'Stracke', 'ujerde@becker.com', '1-283-435-1345', '187 Bartell Glens\nPort Jesus, LA 96724'),
(23, 'Kelley', 'Spencer', 'kling.daphnee@hotmail.com', '+1-406-481-5787', '6775 Antonette Village\nHandfurt, CA 29518'),
(24, 'Jazlyn', 'O\'Hara', 'zrice@ryan.com', '423.395.7305', '3482 Kling Union\nO\'Haraland, NH 42446'),
(25, 'Elsie', 'Padberg', 'hodkiewicz.guillermo@wiegand.com', '781.716.6738', '38466 Roob Ports Suite 906\nFadelview, KY 72532'),
(26, 'D\'angelo', 'Langosh', 'hessel.furman@gmail.com', '(561) 335-0044', '7317 Pagac Islands\nRosaliaton, WV 79787'),
(27, 'Nico', 'Ondricka', 'jrodriguez@hoppe.net', '(260) 972-3112', '1265 Iva Ramp Suite 346\nPiperchester, MA 84279'),
(28, 'Ryan', 'Friesen', 'qrath@gmail.com', '+1.701.246.4772', '5230 Cierra Way\nBerneiceview, SD 82159'),
(29, 'Eliezer', 'King', 'mlang@maggio.com', '1-830-845-7319', '626 Brendon Turnpike Apt. 532\nLake Misty, DE 33533-0061'),
(30, 'Zackary', 'Mayer', 'tiffany15@yahoo.com', '(608) 427-7486', '40244 Greenholt Lights\nCassandreside, SD 32048-6957'),
(31, 'Myrtle', 'Hirthe', 'lgreenfelder@wintheiser.info', '934-602-0024', '288 Daniella Skyway Suite 632\nEast Brannonfort, NC 88215-8951'),
(32, 'Claudia', 'Tromp', 'bgulgowski@nader.com', '785.444.9941', '123 Nia Light Apt. 212\nOrlandborough, OK 47576-5812'),
(33, 'Melyssa', 'Ankunding', 'edgar.heidenreich@funk.com', '+1 (854) 345-7313', '687 Mante Haven\nSchaeferland, HI 58177-8773'),
(34, 'Erika', 'Farrell', 'estevan.cremin@hotmail.com', '+16512772098', '5623 Zakary Lights\nJarrellville, MN 14932'),
(35, 'Stefanie', 'Dickens', 'shanna17@hotmail.com', '(480) 660-4156', '4925 Caesar Lane Suite 753\nPort Lottiechester, OR 31100-6304'),
(36, 'Krystina', 'Hudson', 'rgreenholt@mosciski.com', '651.980.9055', '58874 Charley Villages\nSouth Lorenzaton, NM 97805-2000'),
(37, 'Dalton', 'Shanahan', 'abner.botsford@jacobi.com', '(978) 908-7541', '688 Melisa Manor\nLake Salvadorview, MI 33858-1903'),
(38, 'Maggie', 'Upton', 'jschulist@gmail.com', '+1.201.540.1769', '58076 Runolfsson Hollow Apt. 057\nEast Favianview, ND 03171'),
(39, 'Tremaine', 'Braun', 'tchamplin@gmail.com', '872-489-7020', '3335 Cullen Curve\nNorth Rafael, HI 27019-2169'),
(40, 'Rebeka', 'Rempel', 'bkuvalis@yahoo.com', '+16788233096', '77209 Spencer Mission Suite 336\nWest Crystalview, TX 52998'),
(41, 'Ollie', 'Fritsch', 'misael45@oreilly.biz', '+1-520-486-1943', '492 Price Street Suite 825\nNew Zellatown, AL 06891'),
(42, 'Diana', 'Schamberger', 'emery.paucek@yahoo.com', '+1 (564) 655-0965', '6395 Randal Mountains\nAlysonborough, MS 88807-4933'),
(43, 'Melyssa', 'Walsh', 'wolf.rosanna@smitham.info', '878.324.4997', '5059 Laurence Wall\nEast Trinity, NY 93200'),
(44, 'Joy', 'Keebler', 'william.oconner@gmail.com', '+1-971-520-7350', '45359 Dan Rest\nPort Samantaburgh, AL 32222'),
(45, 'Joany', 'Medhurst', 'zcorwin@ortiz.com', '845-567-9234', '545 Baumbach Overpass\nNorth Unabury, NV 64019-2901'),
(46, 'Dan', 'Smith', 'gkihn@gmail.com', '912.788.3174', '40981 Peter Dale Suite 070\nColbyton, OR 12543'),
(47, 'Marshall', 'Beatty', 'thiel.isaias@hotmail.com', '(516) 987-9386', '79917 Murray Walk Suite 193\nNew Woodrowhaven, TN 50594-2351'),
(48, 'Raul', 'Funk', 'micaela02@feil.org', '+1.651.578.8729', '12031 Howell Streets Suite 900\nAdamsfurt, NM 11111'),
(49, 'Percival', 'Wintheiser', 'konopelski.donny@gmail.com', '+1.812.212.2416', '225 Wilma Canyon\nPort Sabinachester, ID 33092-8753'),
(50, 'Grady', 'Pacocha', 'yundt.russ@hickle.com', '740.492.1155', '9602 Pollich Plains Apt. 920\nHicklebury, CA 60065-8085'),
(51, 'Alana', 'Bradtke', 'keira63@bechtelar.com', '+1-619-714-3957', '755 Annamarie Burgs\nLake Rosemarie, CA 86458-8058'),
(52, 'Hipolito', 'Konopelski', 'haag.tremaine@wisoky.biz', '346.293.0823', '265 Wilhelm Greens Suite 824\nNorth Judah, AK 03120'),
(53, 'Frances', 'Ankunding', 'qgrady@lindgren.com', '(678) 478-6564', '560 Gertrude Mount\nPort Reyna, MN 37654-6686'),
(54, 'Rusty', 'Lubowitz', 'immanuel17@vandervort.org', '+1.856.908.4338', '9326 Christelle Manors\nBorerport, AK 53653-2486'),
(55, 'Lesley', 'Schowalter', 'msipes@hotmail.com', '651-652-4976', '7250 Dickens Manor Apt. 695\nNew Laceyport, OH 99622'),
(56, 'Curtis', 'Deckow', 'predovic.rafael@hotmail.com', '1-352-589-0775', '254 Micheal Radial\nEast Claudie, WA 36743-8596'),
(57, 'Eugene', 'Schamberger', 'robbie88@hotmail.com', '828.358.3003', '2580 Mayer Terrace\nEast Virgil, AR 74578'),
(58, 'Allan', 'Cole', 'hortense95@sauer.com', '1-818-269-6035', '82743 Swaniawski Port\nEast Devinstad, MA 68848-5285');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_amount` decimal(12,2) NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total_amount`, `customer_id`, `date`) VALUES
(1, '632.00', 21, '1983-12-17'),
(2, '666.00', 21, '2000-10-29'),
(3, '615.00', 21, '2003-05-30'),
(4, '617.00', 24, '2003-05-09'),
(5, '619.00', 24, '2003-05-09'),
(6, '647.00', 26, '2003-05-09'),
(7, '607.00', 27, '1982-11-11'),
(8, '634.00', 28, '2016-02-13'),
(9, '615.00', 29, '2004-12-13'),
(10, '660.00', 30, '1992-04-24'),
(11, '641.00', 31, '1988-09-05'),
(12, '606.00', 32, '1973-03-20'),
(13, '617.00', 33, '1978-05-08'),
(14, '679.00', 34, '2011-02-10'),
(15, '684.00', 35, '2007-09-12'),
(16, '680.00', 36, '2015-08-16'),
(17, '678.00', 37, '1977-08-08'),
(18, '692.00', 38, '2020-01-19'),
(19, '629.00', 39, '2006-05-27'),
(20, '625.00', 40, '2003-02-09'),
(21, '635.00', 41, '1986-01-10'),
(22, '651.00', 42, '1975-07-12'),
(23, '600.00', 43, '2014-03-04'),
(24, '652.00', 44, '2009-04-08'),
(25, '647.00', 45, '2009-02-09'),
(26, '646.00', 46, '1972-01-20'),
(27, '604.00', 47, '2022-07-05'),
(28, '672.00', 48, '1981-08-27'),
(29, '675.00', 49, '2022-08-06'),
(30, '638.00', 50, '2016-06-08'),
(31, '685.00', 51, '1979-08-27'),
(32, '601.00', 52, '2005-01-25'),
(33, '606.00', 35, '1974-06-24'),
(34, '601.00', 54, '1999-07-03'),
(35, '624.00', 55, '2017-08-25'),
(36, '614.00', 56, '1987-03-07'),
(37, '680.00', 42, '1994-05-02'),
(38, '675.00', 33, '2014-12-25');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `unit_price` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`order_id`, `product_id`, `quantity`, `unit_price`) VALUES
(3, 31, 1, '376.00'),
(4, 32, 1, '330.00'),
(5, 33, 3, '390.00'),
(6, 34, 1, '465.00'),
(7, 35, 5, '244.00'),
(8, 36, 5, '158.00'),
(9, 37, 3, '400.00'),
(10, 38, 2, '192.00'),
(11, 39, 3, '189.00'),
(12, 40, 5, '352.00'),
(13, 41, 2, '327.00'),
(14, 42, 4, '340.00'),
(15, 43, 5, '141.00'),
(16, 44, 1, '243.00'),
(17, 45, 2, '484.00'),
(18, 46, 2, '485.00'),
(19, 47, 1, '441.00'),
(20, 48, 2, '499.00'),
(21, 49, 1, '129.00'),
(22, 50, 1, '120.00'),
(23, 51, 4, '410.00'),
(24, 52, 1, '459.00'),
(25, 53, 5, '230.00'),
(26, 54, 3, '161.00'),
(27, 55, 4, '344.00'),
(28, 56, 2, '386.00'),
(29, 57, 2, '335.00'),
(30, 58, 3, '213.00'),
(31, 59, 5, '457.00'),
(32, 60, 4, '191.00'),
(1, 61, 2, '263.00'),
(1, 62, 3, '125.00'),
(1, 63, 4, '280.00'),
(2, 64, 5, '110.00'),
(2, 65, 5, '208.00'),
(2, 66, 4, '470.00'),
(33, 67, 7, '80.00'),
(33, 68, 7, '80.00'),
(33, 69, 7, '80.00'),
(34, 70, 7, '80.00'),
(34, 71, 7, '80.00'),
(34, 72, 7, '80.00'),
(35, 73, 9, '69.00'),
(35, 74, 9, '69.00'),
(35, 75, 9, '69.00'),
(36, 76, 9, '69.00'),
(36, 77, 9, '69.00'),
(36, 78, 9, '69.00'),
(37, 79, 9, '70.00'),
(37, 80, 9, '70.00'),
(37, 81, 9, '70.00'),
(38, 82, 9, '70.00'),
(38, 83, 9, '70.00'),
(38, 84, 9, '70.00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `price`, `description`, `supplier_id`) VALUES
(1, 'voluptatem', '354.00', 'Sed ad sapiente molestiae quibusdam ipsa dolor earum. Magnam maiores labore est nemo magni qui et. Amet minima aut nihil.', 1),
(2, 'voluptatem', '494.00', 'Consectetur est quos et voluptatem dolores nihil in sed. Rerum unde unde commodi temporibus.', 2),
(3, 'voluptatem', '217.00', 'Quae non aut perspiciatis illum et. Ullam dicta placeat dignissimos in amet ducimus qui. Cum aut ut et necessitatibus modi. Quia eos dolorum officia quia quia ad eos autem.', 3),
(4, 'dolor', '310.00', 'Nihil architecto molestias vel cumque. Non sapiente odio aut reiciendis ut expedita accusamus. Rerum recusandae omnis minus incidunt aut quia iusto. Aut ad natus consequatur et fugiat modi.', 4),
(5, 'maxime', '376.00', 'Eligendi dolorum omnis explicabo asperiores. Et quisquam culpa non est. Quo nihil optio repellendus ducimus dolorem sunt.', 5),
(6, 'laborum', '314.00', 'Excepturi quod aliquid natus a rerum tempora. Vel occaecati et praesentium ea quam et sequi aut. Perferendis sit veritatis nostrum qui iste.', 6),
(7, 'assumenda', '134.00', 'Sunt aut corporis qui et dolorem. Ea provident pariatur aliquid ullam sunt.', 7),
(8, 'necessitatibus', '282.00', 'Sint nulla blanditiis rerum rerum repellat corporis est suscipit. Quia molestias deleniti aliquid beatae.', 7),
(9, 'voluptatem', '181.00', 'Commodi blanditiis modi expedita molestiae at facilis. Beatae sint reprehenderit rem et. Eum et autem quas inventore dolores.', 9),
(10, 'voluptas', '195.00', 'Magnam quasi eveniet iure sapiente quia non qui sunt. Porro recusandae quae quia dolorum et. Qui voluptatem ea aut veniam eaque dicta.', 10),
(11, 'ratione', '154.00', 'Dolore officiis eos dolores aliquam quia et. Quis aut magnam consequuntur. Cumque nesciunt eaque aut et. Ut velit reprehenderit numquam rerum.', 11),
(12, 'quos', '296.00', 'Dolores qui voluptatem in et officiis ut. Beatae necessitatibus facilis autem quia molestiae nostrum odit rerum. Ratione qui esse illo.', 12),
(13, 'nesciunt', '114.00', 'Nesciunt excepturi quo ut at non deserunt odit. Quaerat maxime sed commodi. Natus rerum eveniet dolorum excepturi.', 13),
(14, 'eaque', '125.00', 'Magni dolores ducimus non et. Et explicabo quo et culpa. Consequuntur voluptates beatae quas architecto eum. Accusantium dolor assumenda repudiandae.', 14),
(15, 'voluptatem', '466.00', 'Ut dolorem sit quis error id provident id. Rem sit aspernatur est nam temporibus.', 6),
(16, 'quos', '243.00', 'Perferendis fuga cum quae harum qui hic sed. Similique commodi ab occaecati dolore accusamus distinctio inventore. Tenetur impedit accusantium unde tenetur nihil debitis et.', 16),
(17, 'voluptatum', '446.00', 'Voluptate voluptatem est vel quasi et quae velit sapiente. Illo possimus quae culpa est est rerum et et. Reiciendis quidem soluta nam nulla a excepturi placeat.', 17),
(18, 'est', '284.00', 'Et quidem in molestiae illum assumenda. Temporibus et quia magni consequuntur. Architecto minus velit at facere illo.', 18),
(19, 'et', '390.00', 'Voluptatem voluptatem fuga autem dolores vitae natus. Illo pariatur necessitatibus officia vero. Maiores non aut quos aliquam est quo perspiciatis ut. Et porro omnis ratione ad sunt ut.', 19),
(20, 'atque', '240.00', 'Expedita iure et alias fuga ut enim reprehenderit. In minima consequatur labore facilis. Nostrum ullam est impedit sequi dignissimos sed.', 20),
(21, 'accusamus', '328.00', NULL, 31),
(22, 'molestiae', '103.00', NULL, 32),
(23, 'consequuntur', '316.00', NULL, 33),
(24, 'et', '321.00', NULL, 34),
(25, 'repudiandae', '312.00', NULL, 35),
(26, 'accusantium', '209.00', NULL, 36),
(27, 'dolor', '191.00', NULL, 37),
(28, 'saepe', '431.00', NULL, 38),
(29, 'corrupti', '461.00', NULL, 39),
(30, 'velit', '306.00', NULL, 40),
(31, 'eos', '376.00', 'Et vel illo quis facere reprehenderit qui sunt adipisci. Expedita in maiores qui ut dolores amet voluptatem. At quibusdam tempore distinctio aut.', 41),
(32, 'provident', '330.00', 'Sit incidunt dolorum quas optio. Et dolorem dolores suscipit error velit veniam veritatis. Omnis aut voluptatem cum consectetur.', 42),
(33, 'tenetur', '390.00', 'Eaque quam dolorem rem nam. Vero illo iusto ut. Iste nemo dolores amet saepe qui.', 43),
(34, 'totam', '465.00', 'Quo consequatur et dolorem. Aliquid in est ratione minima qui esse eius. Rerum eligendi sed ad sapiente sit. Ut similique dolor repudiandae nemo et tempora nisi at.', 44),
(35, 'facilis', '244.00', 'Temporibus magni voluptatem itaque autem minima ut minima dolores. Eveniet magnam voluptas libero ipsum modi.', 45),
(36, 'a', '158.00', 'Modi quis illo dignissimos dolores voluptates. Id voluptatibus corrupti magnam repellat sapiente. Consequatur nulla quam optio accusantium. Eum autem dolor explicabo iste amet quidem optio.', 46),
(37, 'ratione', '400.00', 'Numquam omnis magnam ut quis dolore magni ut. Quis aliquid sit sit laudantium. Quisquam ratione officiis dolor praesentium.', 47),
(38, 'qui', '192.00', 'Quas qui culpa quia molestiae. Voluptatibus et iusto dignissimos delectus debitis qui et. Ut nisi voluptatem et et aliquid pariatur. Sed est tenetur deserunt nemo est incidunt.', 48),
(39, 'minus', '189.00', 'Exercitationem vel et fugit deleniti officiis optio. Nihil odio adipisci non sequi dolores enim dolores. Voluptate architecto maxime esse magni.', 49),
(40, 'qui', '352.00', 'Harum laborum temporibus ut nobis illo. Velit qui delectus consequuntur voluptates vel eum eligendi.', 50),
(41, 'suscipit', '327.00', 'Vel quia voluptatem aut totam est eos. Aut perspiciatis commodi voluptate. Doloremque recusandae quo occaecati corporis.', 51),
(42, 'sint', '340.00', 'Voluptates autem et ea velit nihil. Consequatur cum magnam laudantium non iure in. Maxime omnis sit maiores dolore aut in distinctio. Nemo ad eius fuga aut earum.', 52),
(43, 'omnis', '141.00', 'Ipsa illum et quae expedita ratione exercitationem repellat. Quaerat vel dolores et et ipsam. Voluptates eum et aperiam temporibus assumenda. Nulla quos corrupti enim nulla maxime dolorum ea.', 53),
(44, 'voluptate', '243.00', 'Eius nihil quidem itaque repellat velit et quaerat quia. Et aut ut natus quos. Deleniti aperiam facere commodi quos.', 54),
(45, 'officia', '484.00', 'Excepturi vel quidem consequuntur molestiae quos enim voluptates. Mollitia consequatur velit debitis. Repudiandae doloribus quia non. Quaerat ut recusandae deleniti earum consequatur consequatur.', 55),
(46, 'ut', '485.00', 'Neque magnam omnis est id vitae. Quas tempore rem at minima magni unde sint. Quia quos voluptates eligendi cupiditate enim autem et commodi. Omnis ipsum eligendi enim ut tenetur provident omnis.', 56),
(47, 'harum', '441.00', 'Iure et sapiente sapiente aspernatur quod aspernatur nostrum. Mollitia autem voluptatem rerum vel quibusdam. Qui inventore distinctio corporis at quidem.', 57),
(48, 'quisquam', '499.00', 'Molestiae ratione omnis minima voluptatem quidem porro accusamus. Officiis explicabo voluptate culpa laboriosam sunt. Voluptatum voluptatum aspernatur et aspernatur sint explicabo.', 58),
(49, 'consectetur', '129.00', 'Sequi laudantium incidunt libero tempore sit et quis. Id iste vitae nihil ipsum eum iusto accusamus dolorem. Est recusandae voluptas dolor sit ex. Nihil ut quasi tempore suscipit quo aliquid minus.', 59),
(50, 'velit', '120.00', 'Harum architecto blanditiis quis mollitia libero. Enim qui et voluptates laboriosam voluptas qui. Incidunt hic enim consectetur tempora. Qui quis et necessitatibus animi quam molestiae ea odio.', 60),
(51, 'exercitationem', '410.00', 'Tenetur inventore non sit aspernatur facilis magni. Quas qui aut voluptate atque eos quis.', 61),
(52, 'illo', '459.00', 'Eos ipsam adipisci iure molestiae non qui aut. Nulla vitae quia excepturi magnam quo. Nobis et voluptatem nulla deserunt eum.', 62),
(53, 'illum', '230.00', 'Numquam error cumque debitis recusandae. Dolor totam sapiente earum. Ratione eligendi voluptatum autem et molestiae.', 63),
(54, 'sed', '161.00', 'Nihil repudiandae eum ipsam a et aut sint. Consequatur perspiciatis voluptas unde omnis. Ea voluptas pariatur tenetur id.', 64),
(55, 'repellat', '344.00', 'Odit qui quis veniam incidunt. Doloribus eligendi porro aut animi. Facere possimus voluptates quia id.', 65),
(56, 'sint', '386.00', 'Sunt voluptatem consequatur dolorem quis expedita. Earum odit et nam reiciendis et. Ullam natus et velit ad fugit consequuntur. Et qui et et ut nesciunt nisi atque fuga.', 66),
(57, 'et', '335.00', 'Dicta doloribus ad eveniet suscipit qui veritatis. Est nulla dolorem voluptas commodi dignissimos molestiae at. Natus aut perspiciatis ut. Odio quibusdam possimus occaecati qui id deserunt.', 67),
(58, 'enim', '213.00', 'Sed accusamus quasi dignissimos quia odit aut. Amet eaque qui ipsa odit autem ipsa. Aut aliquam facilis blanditiis qui officia tenetur.', 53),
(59, 'quidem', '457.00', 'Odio amet necessitatibus sed voluptatem itaque quae ipsum. Fuga et non laboriosam ullam ut et eos. Eius rem id vel natus. Eius qui rerum officiis quibusdam animi.', 69),
(60, 'nihil', '191.00', 'Enim ad est in eum minima harum ad. Deleniti magni et rem ut minima sed iure. Voluptatum et numquam rerum adipisci temporibus est ab. Quaerat aut et omnis aperiam.', 70),
(61, 'sint', '263.00', 'Consequatur velit placeat esse voluptate temporibus voluptatibus aspernatur totam. Ipsa et occaecati et reiciendis. Sunt ratione architecto corrupti qui.', 71),
(62, 'illum', '125.00', 'Et quo quia enim non voluptatem ipsum. Consequuntur fugiat unde reiciendis quasi blanditiis quia. Omnis dolor veniam sed quibusdam unde.', 72),
(63, 'aut', '280.00', 'Et aut officiis provident voluptatum. Assumenda laborum dolorum excepturi ea cum.', 73),
(64, 'aut', '110.00', 'Eaque facere sint nihil est deleniti recusandae tenetur. Eveniet ducimus sed eos eos. Aperiam et in voluptatem.', 74),
(65, 'ipsa', '208.00', 'Eum aut praesentium hic delectus quidem et. Tempore deleniti praesentium et mollitia enim quas. Non hic velit quae veniam placeat quod et sit.', 75),
(66, 'quibusdam', '470.00', 'Sit vel rem pariatur. Nam cupiditate velit aut accusantium. Sunt at dignissimos et nihil nam. Suscipit possimus nesciunt aut doloremque.', 76),
(67, 'possimus', '146.00', 'Optio nostrum occaecati in aliquam blanditiis voluptas. Aut illo tempora modi exercitationem perspiciatis dolorem necessitatibus. Qui eum occaecati aut unde aut. Non ducimus nobis ut nesciunt iusto.', 77),
(68, 'autem', '175.00', 'Possimus soluta voluptatum placeat ex et. Nulla rerum facilis facilis deleniti laudantium ratione et. Voluptas rerum culpa ratione sed deleniti qui fugit. Et deserunt officia dolorum vel.', 78),
(69, 'adipisci', '244.00', 'Eos quia necessitatibus aut ut labore nihil voluptatibus. Aut est tenetur eos quia.', 79),
(70, 'quam', '479.00', 'Rerum eum ullam soluta perspiciatis explicabo hic. Voluptatibus nostrum pariatur deserunt eaque iure laborum. Sed accusantium similique adipisci sunt.', 80),
(71, 'dolorem', '110.00', 'Et numquam enim sed repellat veniam neque eveniet. Eos eum voluptas veritatis fugiat ipsa id sequi. Eius culpa ratione esse qui totam voluptatem aut.', 81),
(72, 'ut', '277.00', 'Cupiditate neque odio rerum voluptates neque reiciendis nihil. Quis eum ratione et blanditiis sed. Est soluta ut alias est.', 82),
(73, 'officiis', '474.00', 'Harum voluptas et iste. Rerum ex maiores et dolor labore. Officia aut laboriosam qui id. Rerum molestias quasi odio dolore ratione et.', 83),
(74, 'quas', '218.00', 'Quia velit sit est ut. Asperiores eos laborum qui illum excepturi iusto. Iure est autem temporibus quia eaque.', 84),
(75, 'vel', '198.00', 'Autem soluta praesentium at tenetur ut at voluptatem dignissimos. Nemo non nisi aspernatur aut reiciendis perferendis. Et deleniti et deleniti sint ut.', 85),
(76, 'ut', '262.00', 'Aut id illo iure natus omnis voluptas. Alias corporis ipsa ad occaecati molestiae necessitatibus pariatur. Repudiandae aut dolores quod. Et maiores error accusamus ut quod quisquam.', 86),
(77, 'molestiae', '428.00', 'Quia est qui quo. Ut aut nihil amet quis dolore iure. Omnis accusamus et doloremque aut esse id. Sunt nihil quisquam omnis sunt in vel nemo. Itaque praesentium porro est.', 6),
(78, 'consequatur', '395.00', 'Nesciunt laboriosam repellendus et ut cumque quod beatae. Omnis voluptatem at mollitia quas animi omnis quo perspiciatis.', 88),
(79, 'officiis', '261.00', 'Vel adipisci molestiae rerum accusantium repellendus. Voluptatem omnis eos excepturi et et ducimus. Numquam consequuntur hic et. Quaerat blanditiis officiis eum qui facilis atque atque.', 89),
(80, 'repellat', '128.00', 'Vel rerum eum excepturi at qui tempore reiciendis. Veritatis quo maiores debitis voluptatum sed voluptatem aut. Perferendis et voluptatem doloremque ut.', 90),
(81, 'et', '291.00', 'Repudiandae est quos inventore hic. Minima magnam incidunt est error adipisci saepe. Labore voluptate beatae qui accusamus.', 91),
(82, 'magni', '175.00', 'Id eligendi quod recusandae suscipit libero aperiam. Libero et sed qui dolores molestiae qui est. Non sunt quidem excepturi excepturi. Consectetur culpa provident iure atque mollitia provident.', 92),
(83, 'nihil', '140.00', 'Voluptatem dolorem suscipit minus magni occaecati ab dignissimos. Non culpa eius provident voluptas eos deleniti. Dolorem soluta laboriosam ducimus est. In cupiditate ratione ex itaque consequatur.', 93),
(84, 'incidunt', '443.00', 'Dolores quo accusantium est et dicta neque. Eum velit atque sunt dolores laudantium repellat. Velit officiis culpa architecto pariatur exercitationem.', 94);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_name`, `email`, `phone`, `address`) VALUES
(1, 'Kunze-Mraz', 'terry.royal@brakus.net', '+1-918-208-0208', '37504 Ayana Road Apt. 015\nLake Korytown, VA 64644-6850'),
(2, 'Johnston-Ledner', 'donnie98@grant.com', '1-458-481-8734', '58914 Joel Heights\nSpinkaberg, AL 04155'),
(3, 'Hettinger, Mante and Senger', 'hebert@walter.com', '930-480-8749', '3319 Nikolaus Parks\nElisaville, MA 42365'),
(4, 'Casper, VonRueden and Kemmer', 'garth05@dach.com', '+19595214532', '5500 Berge Green\nEast Candice, IN 80777-7708'),
(5, 'Beatty, Lebsack and Hamill', 'janick.boyer@paucek.info', '+1.947.254.8010', '516 Josue Mission Suite 724\nCaylaside, WA 79046'),
(6, 'Sauer LLC', 'marquis.schneider@daugherty.biz', '520.642.6945', '34858 Schamberger Plain Suite 924\nAliviachester, TN 04240-2289'),
(7, 'Abernathy, Kshlerin and Klocko', 'rdavis@bernier.com', '1-678-635-2366', '7901 Yundt Parkways Apt. 866\nSonnyburgh, HI 79201-1367'),
(8, 'Grady-Lind', 'hhowell@weissnat.info', '(929) 919-7298', '6945 Wyman Route Apt. 766\nMaggiobury, ND 70025-7303'),
(9, 'Armstrong-Wiegand', 'euna.wehner@conn.biz', '+1 (320) 681-0054', '85062 Estevan Key\nCreminshire, AR 51104-0101'),
(10, 'Heller-Russel', 'emmitt28@hyatt.com', '(351) 704-2527', '821 Ellsworth Rapid Suite 372\nAlvisport, UT 92789-7010'),
(11, 'Mohr Inc', 'orval.ziemann@bednar.com', '930.719.4713', '69663 Gottlieb Vista\nPort Letha, NY 52852'),
(12, 'Skiles, Raynor and Cole', 'loraine49@jacobi.com', '+1 (959) 967-3749', '1644 Bahringer Plaza Apt. 137\nJohannview, VT 00984'),
(13, 'Smitham, Stamm and Murphy', 'iarmstrong@kertzmann.com', '(734) 487-5192', '3261 Rippin Mills Apt. 835\nNorth Darron, VA 03275-3974'),
(14, 'Kunze, Reilly and Moore', 'cleffler@kunze.biz', '+1.229.814.9747', '821 Spinka River\nNorth Fanny, OR 08594-4034'),
(15, 'Tremblay, Morissette and Fay', 'rosanna.crist@wunsch.com', '+15103209817', '5424 Turcotte Square\nNew Fritz, ME 96483-8681'),
(16, 'Kshlerin LLC', 'koepp.hilbert@jenkins.com', '417-847-3989', '949 Bogan Expressway Apt. 758\nPort Zanderside, GA 60322'),
(17, 'Hyatt-Bauch', 'hassie75@brown.org', '202.233.6849', '2497 Carter Turnpike\nLake Reganburgh, MN 97869-4855'),
(18, 'Hammes-Brekke', 'demetrius87@rodriguez.com', '559.953.8217', '377 Christelle Spur\nDeloresview, AL 85558'),
(19, 'Hyatt, Farrell and Jakubowski', 'vbaumbach@ebert.biz', '(209) 666-0980', '9900 Cronin Inlet\nFreemanville, TN 18287-6075'),
(20, 'Satterfield-Shields', 'louie.reinger@hill.org', '872-463-0664', '8314 Weimann Ford\nFeeneyfort, MS 04635'),
(21, 'Daugherty Ltd', 'beier.marina@gleason.com', '330.482.2372', '52584 Rau Passage\nLake Arjun, TN 65663'),
(22, 'Stiedemann-Morar', 'jaquelin08@will.com', '930.813.1997', '8067 Hammes Place Apt. 854\nNorth Edwardo, NV 51177'),
(23, 'Champlin, Mosciski and Jaskolski', 'xfisher@torp.biz', '+13609690553', '517 Robel Drive\nStiedemannhaven, SD 99952'),
(24, 'Hilpert-Wunsch', 'cleve47@hodkiewicz.com', '+16574773205', '3687 Labadie Shore Apt. 474\nJayceeton, CT 35687-7587'),
(25, 'Murphy Inc', 'raymundo58@abernathy.com', '(585) 443-9311', '3778 Joshua Mission Suite 490\nSouth Quintonton, GA 03699'),
(26, 'Crooks Group', 'hdenesik@dickens.com', '469-594-2825', '2343 Geo Skyway Suite 321\nOndrickahaven, AR 39496-8011'),
(27, 'Wyman-Yundt', 'christiansen.joanny@hansen.com', '+1-717-627-5880', '53533 Duane Field\nNew Michelle, WV 71406'),
(28, 'Wiza, Harber and Becker', 'mathias.franecki@frami.com', '+1.803.874.0604', '3384 Abdiel Mountains\nToneyside, NE 15669'),
(29, 'Langworth-Powlowski', 'addison32@spinka.biz', '380.657.7817', '32217 Rice Valley\nPort Pinkie, NM 89156'),
(30, 'Hettinger-Hartmann', 'sunny79@yost.com', '+1-716-582-1436', '250 Estell Glen\nWest Helenton, UT 43231'),
(31, 'Mitchell, Schmeler and Armstrong', 'aimee75@bahringer.com', '1-413-810-1955', '333 Morris Rapids\nJoesphmouth, AK 52690-3442'),
(32, 'Mosciski-Hodkiewicz', 'gust06@cartwright.net', '223-747-2242', '79511 Jude Ford Apt. 878\nRettaberg, NH 50724'),
(33, 'Bahringer PLC', 'trenton57@wolff.com', '(540) 913-9711', '480 Freda Ranch Apt. 036\nNew Norrischester, CT 56551-6708'),
(34, 'Glover, Weimann and Bahringer', 'quinten97@renner.com', '445.939.9833', '257 Renner Unions\nJackiemouth, MS 83131-0151'),
(35, 'Tromp, O\'Keefe and Legros', 'nitzsche.matilde@bechtelar.info', '+1-616-772-4255', '23212 Sheridan Keys\nJonatanfurt, MI 91073-6661'),
(36, 'Schowalter, Simonis and Kling', 'xdonnelly@ryan.com', '1-631-719-0102', '58277 Carter Shoals Apt. 258\nBodefurt, CA 94455-4510'),
(37, 'Kerluke and Sons', 'vdaniel@flatley.biz', '(240) 476-7621', '99706 Carter Grove\nSouth Andreanne, NJ 34053'),
(38, 'Kunde Ltd', 'dibbert.mabel@conn.info', '+1-248-778-3088', '775 Autumn Estates\nKieranside, NE 06763'),
(39, 'Fisher, Flatley and Hodkiewicz', 'ima.schuppe@pacocha.com', '+1-937-591-3023', '780 Haag Estates Apt. 441\nSouth Lilianeberg, VA 18221'),
(40, 'Bins-Tremblay', 'qkutch@lubowitz.com', '(253) 564-6441', '2233 Nienow Pine Suite 198\nNicolasfort, WV 20473'),
(41, 'Mertz, Oberbrunner and Donnelly', 'rice.johanna@gerhold.com', '1-971-609-8799', '200 Abernathy Corners\nPort Peyton, VT 06451-3786'),
(42, 'Trantow-Tremblay', 'mpfannerstill@turcotte.net', '(626) 638-5141', '6489 Satterfield Island Suite 628\nNew Quinn, LA 25670'),
(43, 'Hettinger-Windler', 'carolyn.ryan@morar.biz', '+19317329506', '36005 D\'Amore Loaf\nNew Marguerite, FL 10286'),
(44, 'Lindgren, Mraz and Kuhn', 'godfrey55@greenfelder.net', '276-904-5242', '957 Lawson Prairie Suite 789\nFadelhaven, TX 58969-0155'),
(45, 'Lehner, Beatty and Medhurst', 'stella.mosciski@fisher.biz', '+14588770110', '1785 Littel Skyway\nSouth Maxime, ND 61385'),
(46, 'Flatley Ltd', 'cummerata.cleora@langworth.com', '651-231-3430', '44676 Mueller Dam Apt. 041\nNorth Elian, KS 85806-5005'),
(47, 'Heller-Dicki', 'yoconnell@hills.com', '+1-972-771-6965', '45586 Ledner Mills Apt. 997\nCroninland, NH 99127'),
(48, 'Simonis Ltd', 'reese43@metz.org', '+18659140092', '957 Renner Alley Suite 839\nWest Horacio, VA 17966-0908'),
(49, 'Abernathy-Senger', 'claudia97@sawayn.com', '+1.910.367.2370', '1764 Conn Skyway\nWest Sheldon, WI 27727-1941'),
(50, 'Hoeger, Tromp and Daugherty', 'mckayla.mccullough@tremblay.info', '281.647.8641', '286 Rowe Road Suite 777\nLake Keithville, VT 35897'),
(51, 'Toy-McDermott', 'oconner.scot@schiller.com', '+1.623.200.1423', '48017 Kyle Mill Suite 273\nEast Aliza, WV 73434-2857'),
(52, 'Lind and Sons', 'allen.donnelly@gerlach.info', '1-520-271-8154', '490 Frami Gateway Suite 823\nReichertfort, LA 22937'),
(53, 'Auer, Aufderhar and Casper', 'andy.quitzon@feeney.com', '602-720-2970', '9600 Kenyatta Ville\nHowechester, KY 63310'),
(54, 'Steuber, Labadie and Adams', 'lavonne94@ferry.com', '213-644-1699', '64506 Bogan Parks\nNew Elsietown, DE 50715-7174'),
(55, 'Lind LLC', 'milton79@streich.info', '+1.458.446.4670', '51359 Willow Mountain\nLittelport, AK 70733'),
(56, 'Wolff Ltd', 'green.oren@feil.biz', '561-692-5146', '352 Kris Stream Suite 632\nLake Eliseo, NV 78356-1664'),
(57, 'Bayer, O\'Conner and Schumm', 'jblock@terry.net', '+1 (217) 409-3644', '800 Trisha Crest Suite 029\nEast Abagail, NY 06954'),
(58, 'Dicki-Turner', 'littel.thelma@ortiz.com', '404-393-7164', '841 Rory Glen Apt. 652\nPort Eino, VT 45437-3159'),
(59, 'Reinger-Barton', 'hkihn@kunde.com', '845-926-6462', '798 Schinner Pike\nSouth Gailmouth, GA 27437-6215'),
(60, 'Schmidt Group', 'kristin.boehm@waters.com', '620.617.6741', '12518 Felipa Prairie\nNew Creolahaven, NH 95507'),
(61, 'Satterfield, Wiza and Ledner', 'dayton00@hirthe.com', '+1.878.775.2606', '18175 Alford Glen Apt. 961\nAntoniettafurt, IN 31679-3663'),
(62, 'Gleichner-Mayert', 'skyla.tillman@spencer.com', '651-545-3466', '1668 Jailyn Station\nMurazikborough, AK 99339'),
(63, 'Lynch and Sons', 'astrid.little@frami.com', '1-832-749-0234', '916 Morton Oval Apt. 228\nWest Zoeburgh, AL 53344-0350'),
(64, 'Dooley LLC', 'volkman.rachel@mayer.com', '(239) 418-6624', '465 Feeney Greens Apt. 029\nWest Beverly, MO 89126-3471'),
(65, 'Murazik LLC', 'leanne.ferry@shanahan.org', '1-386-372-0692', '421 Dee Light Suite 882\nDeckowmouth, MN 11743-3213'),
(66, 'Hamill Ltd', 'jaskolski.opal@miller.info', '1-463-254-0294', '658 Grayson Passage\nTierrafurt, VT 13709-8335'),
(67, 'Parisian PLC', 'christ.gibson@schuppe.com', '(909) 591-0644', '582 Ullrich Vista\nNew Estrellabury, ND 09748-5624'),
(68, 'Kautzer and Sons', 'madeline.lockman@dare.com', '941-567-3489', '868 Rowe Villages\nSouth Myles, CA 75377-3429'),
(69, 'Trantow LLC', 'alene.nikolaus@hartmann.biz', '580-608-4478', '398 Cordia Burg Suite 492\nLake Ellie, CT 66140'),
(70, 'Tremblay-Renner', 'xdicki@hilpert.com', '(913) 273-1874', '7907 Burdette Spur\nWilliamton, NJ 67567-9697'),
(71, 'Gleichner, Kuhn and Bahringer', 'legros.duncan@leannon.com', '253.223.9477', '367 Bechtelar Gardens\nShanelport, AR 38987'),
(72, 'Wehner, Mann and Abshire', 'vandervort.gisselle@blick.com', '+1-307-667-2497', '3532 Shemar Crescent\nHalvorsonfort, TN 00056-9089'),
(73, 'Howe LLC', 'juston83@gutkowski.com', '+15022183778', '183 Aubrey Center\nPort Audra, ND 52130-1463'),
(74, 'Raynor-Schneider', 'howell.troy@hegmann.com', '385-878-8456', '26907 Joy Manors Suite 690\nNorth Jamey, NJ 87462-1069'),
(75, 'Gutmann-Cole', 'fhyatt@carroll.org', '+1 (412) 746-9128', '7143 Tristian Port\nMillershire, OR 42996'),
(76, 'Padberg LLC', 'christiana16@pfannerstill.com', '253.213.7511', '9098 Gislason Fall\nSouth Moriah, TX 78574-3364'),
(77, 'Heller, Gaylord and Murazik', 'rachelle.skiles@kohler.biz', '1-240-777-2104', '37066 Berenice Club Apt. 516\nWest Maybell, DC 27653'),
(78, 'Satterfield-VonRueden', 'athena25@fritsch.com', '1-423-643-6215', '5237 Doyle Square Apt. 864\nNew Emanuel, FL 10066-5731'),
(79, 'Lehner and Sons', 'connor55@blick.org', '(820) 486-5590', '11812 Jeremie Ridges\nWest Cierra, RI 55044-2167'),
(80, 'Grimes-Hamill', 'odeckow@smith.org', '+1 (430) 877-6376', '223 Wyman Ville Suite 087\nClevelandland, MN 29634'),
(81, 'Daniel-Gleichner', 'rowe.marjolaine@borer.com', '872-394-2596', '467 Hagenes Points\nNorth Donnellbury, VT 50362'),
(82, 'Dickinson, Dach and Lebsack', 'fmante@kessler.com', '+1-838-812-5521', '536 Jakubowski Land Suite 196\nNew Zoieland, AZ 45229-8863'),
(83, 'Crona Group', 'dlubowitz@mraz.com', '320.950.2917', '3072 Kuhn Lock\nNorth Guillermo, WY 28323-7229'),
(84, 'Wilkinson LLC', 'susanna.torp@farrell.biz', '678.453.3810', '8155 Gilbert Gateway Apt. 023\nWest Keegan, SD 63201-4690'),
(85, 'Bartoletti, Gibson and Bogan', 'metz.marie@marquardt.com', '(740) 229-2372', '980 Waters Hollow Suite 173\nGrahamstad, WY 78987-4486'),
(86, 'Bins, Bode and Franecki', 'bhansen@botsford.com', '+12799874236', '55981 Litzy Wall Suite 260\nKoelpintown, FL 20693-9269'),
(87, 'Erdman-Padberg', 'elyssa.lehner@pagac.com', '+1-843-867-8123', '12282 Alanna Garden Suite 166\nThompsonchester, IL 62350-0556'),
(88, 'Ratke, Crist and Ratke', 'dheathcote@pfeffer.com', '406-286-9056', '224 Kris Plaza Suite 109\nDavisport, SC 85668-1780'),
(89, 'Oberbrunner, Champlin and Borer', 'dkovacek@vonrueden.net', '+19497183824', '86787 Lonny Junctions\nColleenmouth, HI 81563-0110'),
(90, 'Zemlak Group', 'anderson.rafael@bernier.com', '+1.347.649.5082', '115 D\'Amore Village\nLake Queenie, NH 82176-2204'),
(91, 'Little-Kiehn', 'tthompson@white.com', '+1 (484) 805-1966', '8778 Hermiston Forge\nTillmanland, OK 37681'),
(92, 'Koss-Prohaska', 'keebler.adolfo@abshire.com', '+1-279-348-9878', '64602 Noah Keys Suite 459\nSouth Delmer, AR 18011'),
(93, 'Nicolas and Sons', 'kade.daniel@champlin.info', '+14802680761', '441 Aisha Track Suite 245\nJamilstad, CA 64821-2376'),
(94, 'Braun Ltd', 'miracle66@reilly.org', '1-520-501-4188', '8234 Ernser Centers Suite 913\nHermannbury, WA 31001-0822');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD KEY `order_items_order_id_foreign` (`order_id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
